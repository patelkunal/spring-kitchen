package org.coderearth.springkitchen.springinpractice.ch01.service;

import org.coderearth.springkitchen.springinpractice.ch01.AbstractTestSuite;
import org.coderearth.springkitchen.springinpractice.ch01.model.Account;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.*;

/**
 * Created by kunal_patel on 14/11/15.
 */
public class AccountServiceImplTest extends AbstractTestSuite {

    private AccountService accountService;

    private final Date today = new Date().from(LocalDate.now().atStartOfDay().toInstant(ZoneOffset.UTC));

    @Before
    public void setUp() throws Exception {
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext(super.appContextRelativePath);
        accountService = applicationContext.getBean("accountService", AccountService.class);
        assertNotNull(this.accountService);
    }

    @After
    public void tearDown() throws Exception {
        accountService = null;
    }

    @Test
    public void testAddAccount() throws Exception {
        accountService.addAccount(new Account("400", new BigDecimal(1000), today));
    }

    @Test
    public void testGetAccount() throws Exception {
        accountService.addAccount(new Account("400", new BigDecimal(1000), today));
        Account account = accountService.getAccount("400");
        assertNotNull(account);
        assertEquals("400", account.getAccountNumber());
        assertEquals(new BigDecimal(1000), account.getAccountBalance());
        assertEquals(today, account.getLastLogin());
        System.out.println(account);
    }

}