package org.coderearth.springkitchen.springinpractice.ch01.dao;

import org.coderearth.springkitchen.springinpractice.ch01.model.Account;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.util.NoSuchElementException;

import static org.junit.Assert.*;

/**
 * Created by kunal_patel on 07/11/15.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/springinpractice/ch01/applicationContext.xml" })
public class InMemoryAccountDaoImplTest {

	@Autowired
	private AccountDao accountDao;

	@Before
	public void setUp() {
		assertNotNull(accountDao);
	}

	@After
	public void tearDown() {
		accountDao = null;
	}

	@Test
	public void testGetAllAccounts() throws Exception {
		assertNotNull(accountDao);
		System.out.println(accountDao);
		System.out.println(accountDao.get());
		assertEquals(3, accountDao.getNumberOfAccounts());
	}

	@Test
	public void testGetSingleAccount() throws Exception {
		assertEquals(new BigDecimal(0), accountDao.get("100").getAccountBalance());
	}

	@Test(expected = NoSuchElementException.class)
	public void testGetSingleAccount_failOnUnknownAccountNumber() throws Exception {
		assertEquals(new BigDecimal(0), accountDao.get("500").getAccountBalance());
	}
}