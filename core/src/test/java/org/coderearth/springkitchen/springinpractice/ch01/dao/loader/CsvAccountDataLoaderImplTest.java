package org.coderearth.springkitchen.springinpractice.ch01.dao.loader;

import org.coderearth.springkitchen.springinpractice.ch01.model.Account;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.util.Collection;

import static org.junit.Assert.*;

/**
 * Created by kunal_patel on 07/11/15.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:/springinpractice/ch01/applicationContext.xml"})
public class CsvAccountDataLoaderImplTest {

    @Autowired
    private AccountDataLoader accountDataLoader;

    @Before
    public void setUp() throws Exception {
        assertNotNull(this.accountDataLoader);
    }

    @After
    public void tearDown() throws Exception {
        this.accountDataLoader = null;
    }

    @Test
    public void testSetCsvFileResource() throws Exception {

    }

    @Test
    public void testLoadAccountData() throws Exception {

        Collection<Account> accountCollection = this.accountDataLoader.loadAccountData();
        assertEquals(3, accountCollection.size());
        assertEquals(new BigDecimal(0), accountCollection.iterator().next().getAccountBalance());
        // System.out.println(accountCollection);
    }
}