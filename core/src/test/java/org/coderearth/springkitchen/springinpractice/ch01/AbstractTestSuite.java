package org.coderearth.springkitchen.springinpractice.ch01;

/**
 * Created by kunal_patel on 14/11/15.
 */
public class AbstractTestSuite {

    private static final String appContextFileName = "applicationContext.xml";

    protected static final String appContextRelativePath = "springinpractice/ch01/" + appContextFileName;


}
