package org.coderearth.springkitchen.springinpractice.ch01.model;

import com.google.common.base.MoreObjects;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by kunal_patel on 07/11/15.
 */
public class Account {

    private String accountNumber;
    private BigDecimal accountBalance;
    private Date lastLogin;

    public Account() {
    }

    public Account(String accountNumber, BigDecimal accountBalance, Date lastLogin) {
        this.accountBalance = accountBalance;
        this.accountNumber = accountNumber;
        this.lastLogin = lastLogin;
    }

    public BigDecimal getAccountBalance() {
        return accountBalance;
    }

    public void setAccountBalance(BigDecimal accountBalance) {
        this.accountBalance = accountBalance;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public Date getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(Date lastLogin) {
        this.lastLogin = lastLogin;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("accountNumber", accountNumber)
                .add("accountBalance", accountBalance)
                .add("lastLogin", lastLogin).toString();
    }
}
