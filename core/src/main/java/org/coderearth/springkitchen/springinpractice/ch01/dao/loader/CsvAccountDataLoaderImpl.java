package org.coderearth.springkitchen.springinpractice.ch01.dao.loader;

import org.coderearth.springkitchen.springinpractice.ch01.model.Account;
import org.springframework.core.io.Resource;

import java.io.BufferedReader;
import java.io.FileReader;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Created by kunal_patel on 07/11/15.
 */
public class CsvAccountDataLoaderImpl implements AccountDataLoader {

    private static final DateFormat fmt = new SimpleDateFormat("MMddyyyy");
    private Resource csvFileResource;

    public CsvAccountDataLoaderImpl() {
    }

    public CsvAccountDataLoaderImpl(Resource csvFileResource) {
        this.setCsvFileResource(csvFileResource);
    }

    public void setCsvFileResource(Resource csvFileResource) {
        this.csvFileResource = csvFileResource;
    }

    @Override
    public Collection<Account> loadAccountData() throws Exception {
        List<Account> results = new ArrayList<Account>();
        BufferedReader br = new BufferedReader(new FileReader(csvFileResource.getFile()));
        String line;
        while ((line = br.readLine()) != null) {
            String[] fields = line.split(",");
            String accountNo = fields[0];
            BigDecimal balance = new BigDecimal(fields[1]);
            Date lastPaidOn = fmt.parse(fields[2]);
            Account account = new Account(accountNo, balance, lastPaidOn);
            results.add(account);
        }
        br.close();
        return results;
    }
}
