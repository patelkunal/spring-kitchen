package org.coderearth.springkitchen.springinpractice.ch01.dao.loader;

import org.coderearth.springkitchen.springinpractice.ch01.model.Account;

import java.util.Collection;

/**
 * Created by kunal_patel on 07/11/15.
 */
public interface AccountDataLoader {

    Collection<Account> loadAccountData() throws Exception;

}
