package org.coderearth.springkitchen.springinpractice.ch01.service;

import org.coderearth.springkitchen.springinpractice.ch01.model.Account;

/**
 * Created by kunal_patel on 14/11/15.
 */
public interface AccountService {

    public void addAccount(final Account anAccount);

    public Account getAccount(final String accountId);

    /**
     * Just a sample function for search functionality which expects one of the fields like name, phone number,
     * email address etc.
     *
     * @param accountHolderName
     * @return
     */
    public Account searchAccount(final String accountHolderName);

}
