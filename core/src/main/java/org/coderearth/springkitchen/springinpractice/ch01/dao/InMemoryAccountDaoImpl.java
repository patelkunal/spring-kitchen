package org.coderearth.springkitchen.springinpractice.ch01.dao;

import com.google.common.base.MoreObjects;
import org.coderearth.springkitchen.springinpractice.ch01.dao.loader.AccountDataLoader;
import org.coderearth.springkitchen.springinpractice.ch01.model.Account;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by kunal_patel on 07/11/15.
 */
public class InMemoryAccountDaoImpl implements AccountDao {

    private AccountDataLoader accountDataLoader;

    private AccountsWrapper accounts = new AccountsWrapper();

    public static AccountDao createInMemoryAccountDao(final AccountDataLoader dataLoader) throws Exception {
        InMemoryAccountDaoImpl accountDao = new InMemoryAccountDaoImpl();
        accountDao.setAccountDataLoader(dataLoader);
        accountDao.init();
        return accountDao;
    }

    public AccountDataLoader getAccountDataLoader() {
        return accountDataLoader;
    }

    public void setAccountDataLoader(AccountDataLoader dataLoader) {
        this.accountDataLoader = dataLoader;
    }

    private void init() throws Exception {
        for (Account anAccount : this.getAccountDataLoader().loadAccountData()) {
            this.accounts.addAnAccount(anAccount);
        }
    }

    @Override
    public Collection<Account> get() {
        return this.accounts.getAccountList();
    }

    @Override
    public Account get(String accountId) {
        return this.accounts.getAccountList().stream().filter((anAccount) -> anAccount.getAccountNumber().equalsIgnoreCase(accountId)).findFirst().get();
    }

    @Override
    public int getNumberOfAccounts() {
        return this.accounts.size();
    }

    @Override
    public void add(final Account anAccount) {
        this.accounts.addAnAccount(anAccount);
    }



    @Override
    public String toString() {
        return MoreObjects.toStringHelper("AccountDao")
                .add("type", "InMemoryAccountDao")
                .add("count", this.accounts.size())
                .toString();
    }

    private class AccountsWrapper {

        private List<Account> accountList;

        public AccountsWrapper() {
            accountList = new ArrayList<Account>();
        }

        public List<Account> getAccountList() {
            return accountList;
        }

        public void setAccountList(List<Account> accountList) {
            this.accountList = accountList;
        }

        public void addAnAccount(final Account aAccount) {
            this.accountList.add(aAccount);
        }

        public int size() {
            return this.accountList.size();
        }

    }
}
