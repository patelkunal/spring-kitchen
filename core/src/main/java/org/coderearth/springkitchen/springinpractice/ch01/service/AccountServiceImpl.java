package org.coderearth.springkitchen.springinpractice.ch01.service;

import org.coderearth.springkitchen.springinpractice.ch01.dao.AccountDao;
import org.coderearth.springkitchen.springinpractice.ch01.model.Account;

/**
 * Created by kunal_patel on 14/11/15.
 */
public class AccountServiceImpl implements AccountService {

    private AccountDao accountDao;

    @Override
    public void addAccount(Account anAccount) {
        accountDao.add(anAccount);
    }

    @Override
    public Account getAccount(String accountId) {
        return accountDao.get(accountId);
    }

    /**
     * TODO - not yet implement
     *
     * @param accountHolderName
     * @return
     */
    @Override
    public Account searchAccount(String accountHolderName) {
        return null;
    }

    public void setAccountDao(AccountDao accountDao) {
        this.accountDao = accountDao;
    }
}
