package org.coderearth.springkitchen.springinpractice.ch01.dao;

import org.coderearth.springkitchen.springinpractice.ch01.model.Account;

import java.util.Collection;

/**
 * Created by kunal_patel on 07/11/15.
 */
public interface AccountDao {

    Collection<Account> get();

    Account get(final String accountId);

    int getNumberOfAccounts();

    public void add(Account anAccount);
}
